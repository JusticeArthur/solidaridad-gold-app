<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     schema="NewRegion",
 *     required={"name","country_id"},
 *     @OA\Property(
 *          property="name",
 *          type="string",
 *          description="Region's Name",
 *          example="Volta Region"
 *    ),
 *     @OA\Property(
 *          property="country_id",
 *          type="integer",
 *          description="Id Of it's related country",
 *          example="1"
 *    )
 * )
 * @OA\Schema(
 *     schema="Region",
 *     allOf={
 *     @OA\Schema(ref="#/components/schemas/NewRegion"),
 *     @OA\Schema(
 *       @OA\Property(property="id", type="integer"),
 *       @OA\Property(property="created_at", type="string",format="date"),
 *     )
 *     }
 * )
 */
class Region extends Model
{
   // use Searchable;

    protected $guarded = [];
    public function country(){
        return $this->belongsTo(Country::class);
    }
}
