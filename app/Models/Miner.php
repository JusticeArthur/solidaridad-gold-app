<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Laravel\Scout\Searchable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;


class Miner extends Authenticatable
{
    use Notifiable,HasRoles,HasApiTokens;
   // use Searchable;
    protected $guard_name = 'miners';
    protected $guard = 'miners';
    protected $with = ['roles','permissions'];

    protected$guarded = [];

    public function contacts(){
        return $this->morphMany(Contact::class,'model');
    }
}
