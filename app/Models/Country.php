<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //use Searchable;

    public function regions(){
        return $this->hasMany(Region::class);
    }

}
