<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;

class Mine extends Model
{
    //use Searchable;


    protected $guarded = [];
    public function manager()
    {
       return $this->belongsTo(User::class, 'manager_id', 'id');
    }

    public function community()
    {
       return $this->belongsTo(Community::class, 'community_id', 'id');
    }

}
