<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pillar extends Model
{
    protected $appends = ['color','icon'];
    public function country()
    {
           return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function topics(){
        return $this->hasMany(Topic::class,'pillar_id');
    }

    public function getIconAttribute(){
        switch ($this->standard){
            case 'land':
                return 'environment';
            case 'crop':
                return 'tractor';
            case 'business':
                return 'money';
            case 'people':
                return 'group';
            default:
                return '#fff';
        }
    }
    public function getColorAttribute(){
        switch ($this->standard){
            case 'land':
                return '#774F38';
            case 'crop':
                return '#519548';
            case 'business':
                return '#FF8518';
            case 'people':
                return '#00B4CC';
            default:
                return '#fff';
        }
    }
}
