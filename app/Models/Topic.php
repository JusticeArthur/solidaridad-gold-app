<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $appends = ['loaded_questions','poster','quiz'];
    public function questions(){
        return $this->hasMany(Question::class);
    }
    public function getLoadedQuestionsAttribute(){
        $questions = $this->questions()->pluck('id');
        return Content::query()->whereIn('question_id',$questions)->latest()->get();
    }
    public function getQuizAttribute(){
        $questions = $this->questions()->pluck('id');
        $content =  Content::query()->whereIn('question_id',$questions)->latest()->pluck('question_id');
        return $this->questions()->with(['responses','task'])->whereIn('id',$content)->get();
    }
    public function getAliasAttribute($value){
        return strtolower($value);
    }
    //
    public function pillars()
    {
       return $this->belongsTo(Pillar::class, 'pillar_id', 'id');
    }

    public function getPosterAttribute(){
        if($this->image){
            $names = explode('/',$this->image);
            return asset("images/$names[1]");
        }
        return null;
    }

}
