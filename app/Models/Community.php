<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Community extends Model
{
    //
    public function getNameAttribute($value){
        return Str::title($value);
    }
    protected $guarded = [];
    public function district()
    {
        return $this->belongsTo(District::class);
    }
}
