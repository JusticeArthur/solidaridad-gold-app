<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class District extends Model
{
   // use Searchable;
    protected $guarded = [];

    public function getNameAttribute($value){
        return Str::title($value);
    }
    public function region()
    {
        # code...
       return $this->belongsTo(Region::class);
    }
}
