<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssociatedTask extends Model
{
    protected $guarded = [];
}
