<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function task(){
        return $this->hasOne(AssociatedTask::class);
    }

    public function responses(){
        return $this->hasMany(Response::class);
    }
}
