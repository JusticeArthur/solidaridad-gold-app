<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function model()
    {
        return $this->morphTo();
    }
    protected $guarded = [];
}
