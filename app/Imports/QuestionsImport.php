<?php

namespace App\Imports;

use App\Models\AssociatedTask;
use App\Models\Question;
use App\Models\Response;
use App\Models\Topic;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class QuestionsImport implements ToModel , WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $topic = Topic::query()->whereRaw('lower(name) = (?)',strtolower(trim($row['topic_name'])))->first();
        $responses = [];
        if($row['possible_responses']){
            $responses = preg_split("/[\s,]+/", trim($row['possible_responses']));
        }
        try{
            $question = null;
            if($topic){
                $question = $topic->questions()->create([
                    'question' => $row['question_text'],
                    'question_type' => $row['question_type'],
                    'strong_point' => $row['strong_point'],
                    'weak_point' => $row['weak_point']
                ]);
                //create associated task
                if(trim($row['associated_task'])){
                    $task = [
                        'question_id' => $question->id,
                        'task' => trim($row['associated_task'])
                    ];
                    if(Arr::has($row, 'priority')){
                        $task['priority'] = strtolower(trim($row['priority']));
                    }
                    AssociatedTask::create($task);
                }
                if(count($responses) > 0){
                    foreach ($responses as $response){
                        $resp = [
                            'question_id' => $question->id,
                            'possible_response' => trim($response)
                        ];
                        if(strtolower(trim($response)) === strtolower(trim($row['expected_answer']))){
                            $resp['correct'] = true;
                        }
                        Response::create($resp);
                    }
                }else{
                    if($question){
                        $ans = [];
                        switch (strtolower(trim($row['question_type']))){
                            case 'adherence':
                                $ans = ['Yes','No'];
                                break;
                        }

                        foreach ($ans as $response){
                            $resp = [
                                'question_id' => $question->id,
                                'possible_response' => trim($response)
                            ];
                            if(strtolower(trim($response)) === strtolower(trim($row['expected_answer']))){
                                $resp['correct'] = true;
                            }
                            Response::create($resp);
                        }
                    }
                }

            }
        }catch (\Exception $e){

        }
        /*return new Question([
            //
        ]);*/
    }
}
