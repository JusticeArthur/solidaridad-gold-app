<?php

namespace App\Http\Controllers\V1\Services;

use App\Http\Controllers\Controller;
use App\Models\Pillar;
use Illuminate\Http\Request;

class LoadUserInfoController extends Controller
{
    public function loadPillars(){
        return $this->showOne(Pillar::with(['topics'])->get());
    }
}
