<?php

namespace App\Http\Controllers\V1\Test;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    /**
     * @OA\GET(
     *     path="/users",
     *     tags={"users"},
     *     summary="Returns a the users in the application",
     *     description="This returns all the users in the application",
     *     operationId="users",
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function users(){
        return User::all();
    }
}
