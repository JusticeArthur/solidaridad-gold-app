<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\MineManager;
use App\Models\Miner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/sign-in",
     *      summary="Get list of regions",
     *      description="Returns list of regions",
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       )
     *
     *
     * Returns list of projects
     */
    public function signIn(Request $request){
        $rules = [
            'username'=>'required',
            'password'=>'required'
        ];
        $this->validate($request,$rules);
        //
        //attempt to log the user in
        if(Auth::attempt(['username'=>$request->username,'password'=>$request->password])){
            $user = auth()->user();
            return $this->createAccessToken($user);
        }
        //check if the person is a miner
        $miner = Miner::where('email',$request['username'])->orWhere('username',$request['username'])->first();
        if($miner){
            if(Hash::check($request['password'], $miner->password)){
                Auth::login($miner);
                return $this->createAccessToken($miner);
            }
        }
        //check the telephone number
        $phone = str_replace(' ','',$request['username']);
        $contacts = Contact::query()->cursor()->filter(function ($contact) use ($phone){
           // dd(str_replace('0','',$contact->contact,$count));
            $searchItems = [
                str_replace(' ','',$contact->contact),
                str_replace(' ','',$contact->prefix. $contact->contact),
                str_replace(' ','','+'.$contact->prefix. $contact->contact),
            ];
            if($contact->contact[0] == 0){
                array_push($searchItems,preg_replace('/0/', '', str_replace(' ','',$contact->contact), 1));
                array_push($searchItems,preg_replace('/0/', '', str_replace(' ','',$contact->prefix.$contact->contact), 1));
                $finalCode = '+'.$contact->prefix.preg_replace('/0/', '', str_replace(' ','',$contact->contact), 1);
                array_push($searchItems,$finalCode);
            }
            return Str::contains($phone,$searchItems);
        });
        if(count($contacts) > 0){
            foreach ($contacts as $contact){
                if(Hash::check($request['password'],$contact->model->password)){
                    Auth::login($contact->model);
                    return $this->createAccessToken($contact->model);
                }
            }
        }

        return $this->errorResponse('Your credentials do not match our records',423);

    }

    public function verifyPhone(Request $request){
        $contacts = Contact::whereContact($request->contact)->get();
        if(count($contacts) > 0){
            return $this->showOne(false);
        }
        return $this->showOne(true);
    }

    public function verifyEmail(Request $request){
        $users = Miner::where('email',$request->email)->get();
        if(count($users) > 0){
            return $this->showOne(false);
        }
        return $this->showOne(true);
    }

    public function registerMiner(Request $request){
        $miner = new Miner();
        $miner->first_name = $request['first_name'];
        $miner->last_name = $request['last_name'];
        $miner->gender = $request['gender'];
        $miner->password = bcrypt($request['password']);
        $miner->dob = Carbon::parse($request['dob'])->toDateString();
        $miner->username = strtolower($request['first_name']) .'.'.strtolower($request['last_name']) .rand(1,400);

        if($request['mode'] === 'username'){
            $miner->save();
            //save contact here
            $miner->contacts()->create([
                'contact' => $request['phone'],
                'prefix' => $request['prefix']
            ]);
        }else{
            $miner->email = $request['email'];
            $miner->save();
        }
        //assign role miner
        $role = Role::whereName('miner')->first();
        $miner->assignRole($role);
        //create the associated mine
        MineManager::create([
            'miner_id' => $miner->id,
            'mine_id' => $request['mine_id']
        ]);
        //create a token for the miner
        return $this->createAccessToken(Miner::find($miner->id));
    }

    protected function createAccessToken($user){
        $token = $user->createToken('goldApp');
        $data['token']= $token->accessToken;
        $data['token_expiry'] = $token->token->expires_at;
        $data['user'] = $user;
        return $this->showOne($data);
    }


    public function logout(){
        //accessTokens
        if(auth()->check()){
            auth()->user()->token()->revoke();
        }
        return $this->showOne(['success'],200);
    }
}
