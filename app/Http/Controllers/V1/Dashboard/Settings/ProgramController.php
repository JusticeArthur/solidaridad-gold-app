<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
           return $this->showOne(Program::with('users')
                ->orderBy('created_at','desc')
               ->paginate($request->size));
        }
        $searchItems = Program::search($request['search'])->get()->pluck('id');
        return $this->showOne(Program::with('users')->whereIn('id',$searchItems)->latest()->paginate($request['size']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if (Program::whereRaw('lower(name) like (?)',strtolower($request->name))->get()->isEmpty() ){
        $program = new Program();
        $program->name = $request->name;
        $program->manager_id = $request->manager_id;
        $program->description = $request->description;
        $program->code =$request->code;
        $program->save();
        
           } else {
            // return 'not available'; 
            return $request;
           }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('programs')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'manager_id' => $request->manager_id,
            'description'=> $request->description,
            'code'=>$request->code
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Program::find($id)->delete();
        return $this->showOne('success');
    }
}
