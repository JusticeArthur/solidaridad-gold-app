<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\Mine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
            return $this->showOne(Mine::with(['community','community.district','community.district.region','community.district.region.country'])
                ->orderBy('created_at','desc')
                ->paginate($request->size));
        }
        $searchItems = Mine::search($request['search'])->get()->pluck('id');
        return $this->showOne(Mine::with('community')->whereIn('id',$searchItems)->latest()->paginate($request['size']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $mine = new Mine();
        $mine->name=$request->name;
        $mine->address=$request->address;
        $mine->community_id=$request->community_id;
        $mine->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('MINES')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'address'=> $request->address,
            'community_id'=> $request->community_id,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Mine::find($id)->delete();
        return $this->showOne('success');
    }
    
}
