<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        //
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
            return $this->showOne(Role::with('permissions')
                ->orderBy('created_at','desc')
                ->paginate($request->size));
         }
         $searchItems = Role::search($request['search'])->get()->pluck('id');
         return $this->showOne(Role::whereIn('id',$searchItems)->latest()->paginate($request['size']));
 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $role = Role::create(['name' => $request->name]);
        $permissions = explode(',',$request->permission);
        $role->syncPermissions($permissions);
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('ROLES')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::find($id);
        $role->syncPermissions();
        $role->delete();
    }
}
