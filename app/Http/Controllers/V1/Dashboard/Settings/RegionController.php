<?php

namespace App\Http\Controllers\V1\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionController extends Controller
{

    /**
     * @OA\Get(
     *     path="/dashboard/settings/regions",
     *     tags={"regions"},
     *     summary="List all regions",
     *     @OA\Response(
     *          response=200,
     *          description="List of employees",
     *          @OA\Schema(ref="#/components/schemas/Region")
     *      ),
     *     @OA\Response(
     *          response="default",
     *          description="error",
     *          @OA\Schema(ref="#/components/schemas/Error")
     *   )
     * ),
     */
    public function index(Request $request)
    {
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
            return $this->showOne(Region::with('country')
                ->orderBy('created_at','desc')
                ->paginate($request->size));
        }
        $searchItems = Region::search($request['search'])->get()->pluck('id');
        return $this->showOne(Region::with('country')->whereIn('id',$searchItems)->latest()->paginate($request['size']));

    }

    /**
     * @OA\Post(
     *     path="/dashboard/settings/regions",
     *     tags={"regions"},
     *     summary="Create new region",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(ref="#/components/schemas/Region"),
     *     ),
     *     @OA\RequestBody(
     *         description="order placed for purchasing th pet",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/NewRegion")
     *     )
     * ),
     */
    public function store(Request $request)
    {
        Region::create($request->all());
        return $this->showOne('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Region::where('id',$id)->update($request->all());
        return $this->showOne('success');
    }

    /**
     * @OA\Delete(
     *     path="/dashboard/settings/regions/{id}",
     *     tags={"regions"},
     *     summary="Deletes a region",
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @OA\Parameter(
     *         description="order placed for purchasing th pet",
     *         required=true,
     *          name="id",
     *          in="path",
     *     )
     * ),
     */
    public function destroy($id)
    {
        Region::find($id)->delete();
        return $this->showOne('success');
    }
}
