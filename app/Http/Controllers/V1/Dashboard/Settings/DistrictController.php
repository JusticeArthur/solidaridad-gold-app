<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
            return $this->showOne(District::with(['region','region.country'])
                ->orderBy('created_at','desc')
                ->paginate($request->size));
        }
        $searchItems = District::search($request['search'])->get()->pluck('id');
        return $this->showOne(District::with(['region','region.country'])->whereIn('id',$searchItems)->latest()->paginate($request['size']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        if (District::where('region_id',$request->region_id)->whereRaw('lower(name) like (?)',strtolower($request->name))->get()->isEmpty() ){
        
        
         $district = new District();
       $district->name = $request->name;
        $district->region_id = $request->region_id;
        $district->save();
        } else {
         // return 'not available'; 
         return $request;
        }
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('districts')
              ->where('id', $id)
              ->update([
                  'name' => $request->name,
                  'region_id' => $request->region_id
                  ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        District::find($id)->delete();
        return $this->showOne('success');
    }
}
