<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\Pillar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PillarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->has('size')){
            $is_searchable = $request->has('search') && strlen($request['search']) > 0;
            if(!$is_searchable){
                return $this->showOne(Pillar::with('country')->orderBy('created_at','desc')
                    ->paginate($request->size));
            }
            $searchItems = Pillar::search($request['search'])->get()->pluck('id');
            return $this->showOne(Pillar::whereIn('id',$searchItems)->latest()->paginate($request['size']));
        }
        return $this->showOne(Pillar::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if (Pillar::where('country_id',$request->country_id)->whereRaw('lower(standard) like (?)',strtolower($request->standard))->get()->isEmpty() ){

            $pillar = new Pillar();
        $pillar->name = $request->name;
        $pillar->standard = $request->standard;
        $pillar->country_id = $request->country_id;
        $pillar->save();
               } else {
                // return 'not available';
                return $request;
               }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('pillars')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'standard'=> $request->standard,
            'country_id'=>$request->country_id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Pillar::find($id)->delete();
        return $this->showOne('success');
    }
}
