<?php

namespace App\Http\Controllers\V1\Dashboard\Settings;

use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Country;
use App\Models\District;
use App\Models\EducationalLevel;
use App\Models\Mine;
use App\Models\Pillar;
use App\Models\Program;
use App\Models\Region;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class MiscController extends Controller
{
    public function countries(Request $request){
        if($request->has('paginate')){
            return $this->showOne(Country::paginate(40));
        }
        return $this->showOne(Country::all());
    }

    public function country(Request $request){
        return $this->showOne(Country::query()
            ->where('name','like',"%{$request['name']}%")
            ->where('code','like',"%{$request['code']}%")
            ->first());
    }

    public function exportRegions(){
        return Region::all();
    }

    //get regions of a country
    public function regions($id)
    {
        return $this->showOne(Region::where('country_id',$id)->get());
    }
    
    //get all users
    public function users()
    {
        return $this->showOne(User::all());
    }

    //get all pillars
    public function pillars($id)
    {
        return $this->showOne(Pillar::where('country_id',$id)->get());
    }

    //get districts of a region
    public function districts($id)
    {
        return $this->showOne(District::where('region_id',$id)->get());
    }

    public function mines(Request $request)
    {
        if ($request->has('paginate')) {
            return $this->showOne(Mine::with('community', 'community.district')->orderBy('name')->paginate(10));
        }
    }

    //get all permission
    public function permissions()
    {
        return $this->showOne(Permission::all());
    }

    //get communities in a district
    public function community($id)
    {
        return $this->showOne(Community::where('district_id',$id)->get());
    }
  
    //get topics of pillar
    public function topics($id)
    {
        return $this->showOne(Topic::where('pillar_id',$id)->get());
    }

    public function exportMines(){
        return Mine::all();
    }

    public function exportUsers(){
        return User::all();
    }

    public function exportDistricts(){
        $districts = DB::table('districts')
                       ->leftJoin('regions','districts.region_id','regions.id')
                       ->leftJoin('countries','regions.country_id','countries.id')
                       ->select('districts.name','regions.name as region name','countries.name as country')
                       ->get();
        return $districts;
    }

    public function exportCommunities(){
        $community = DB::table('communities')
                       ->leftJoin('districts','communities.district_id','districts.id')
                       ->leftJoin('regions','districts.region_id','regions.id')
                       ->leftJoin('countries','regions.country_id','countries.id')
                       ->select('communities.name as name','districts.name as district','regions.name as region name','countries.name as country')
                       ->get();
        return $community;
    }

    public function exportPillars(){
        $pillars = DB::table('pillars')
                       ->leftJoin('countries','pillars.country_id','countries.id')
                       ->select('pillars.name as name','pillars.standard as standard','countries.name as country')
                       ->get();
        return $pillars;
    }

    public function exportPrograms(){
        $programs = DB::table('programs')
                       ->leftJoin('users','programs.manager_id','users.id')
                       ->select('programs.name as name','programs.code as code',DB::raw('CONCAT(users.first_name," ",users.last_name) as manager'),'programs.description as description')
                       ->get();
        return $programs;
    }

    public function exportTopics(){
        $topics = DB::table('topics')
                       ->leftJoin('pillars','topics.pillar_id','pillars.id')
                       ->leftJoin('countries','pillars.country_id','countries.id')
                       ->select('topics.name','pillars.name as pillar','countries.name as country','topics.description as description')
                       ->get();
        return $topics;
    }


    public function exportEducations(){
        $education = DB::table('educational_levels')
                       ->select('educational_levels.name as name','educational_levels.description as description')
                       ->get();
        return $education;
    }

    public function count()
    {
        $count = ['district'=>0,'mines'=>0,'regions'=>0,'topics'=>0,'pillars'=>0,'communities'=>0,'edulevels'=>0,'programs'=>0];
        $count['district'] = District::all()->count();
        //$count['mines'] = Mine::all()->count();
        $count['regions'] = Region::all()->count();
        $count['topics'] = Topic::all()->count();
        $count['pillars'] = Pillar::all()->count();
        $count['communities'] = Community::all()->count();
        $count['edu-levels'] = EducationalLevel::all()->count();
        $count['programs'] = Program::all()->count();
        
        return $count;
    }
}
