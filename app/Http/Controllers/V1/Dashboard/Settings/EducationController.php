<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\EducationalLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        //
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
           return $this->showOne(EducationalLevel::orderBy('created_at','desc')
               ->paginate($request->size));
        }
        $searchItems = EducationalLevel::search($request['search'])->get()->pluck('id');
        return $this->showOne(EducationalLevel::whereIn('id',$searchItems)->latest()->paginate($request['size']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (EducationalLevel::whereRaw('lower(name) like (?)',strtolower($request->name))->get()->isEmpty()) {
        
           $education = new EducationalLevel();
        $education->name = $request->name;
        $education->description = $request->description;
        $education->save();
           }else {
              return $request;
           }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        DB::table('educational_levels')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'description'=> $request->description,
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        EducationalLevel::find($id)->delete();
        return $this->showOne('success');
    }
}
