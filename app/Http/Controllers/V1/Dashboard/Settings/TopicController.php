<?php

namespace App\Http\Controllers\V1\Dashboard\settings;

use App\Http\Controllers\Controller;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request$request)
    {
        //
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
           return $this->showOne(Topic::with(['pillars','pillars.country'])
               ->orderBy('created_at','desc')
               ->paginate($request->size));
        }
        $searchItems = Topic::search($request['search'])->get()->pluck('id');
        return $this->showOne(Topic::whereIn('id',$searchItems)->latest()->paginate($request['size']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //

        if (Topic::where('pillar_id',$request->pillar_id)->whereRaw('lower(name) like (?)',strtolower($request->name))->get()->isEmpty() ){
        $topic = new Topic();
        $topic->name = $request->name;
        $topic->pillar_id = $request->pillar_id;
        $topic->description = $request->description;
          if($request->hasFile('pic'))
            {
              $current = time();
              $path = 'images/'.$current.'.'.$request->file('pic')->getClientOriginalExtension();
              Image::make($request->file('pic'))->resize(300, 200)->save($path);    
            }
        $topic->image = $path;
        $topic->save();
           } else {
            // return 'not available'; 
            return $request;
           }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//$path =null;
        if($request->hasFile('pic'))
            {
              $current = time();
              $path = 'images/'.$current.'.'.$request->file('pic')->getClientOriginalExtension();
              Image::make($request->file('pic'))->resize(300, 200)->save($path);    
              DB::table('topics')
              ->where('id', $id)
              ->update([
                  'name' => $request->name,
                  'pillar_id'=>$request->pillar_id,
                  'image'=>$path,
                  'description'=> $request->description,
                  ]);
               
            }else {
              DB::table('topics')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'pillar_id'=>$request->pillar_id,
            'description'=> $request->description,
            ]);  
            }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Topic::find($id)->delete();
        return $this->showOne('success');
    }
}
