<?php

namespace App\Http\Controllers\V1\Dashboard\Content;

use App\Http\Controllers\Controller;
use App\Repositories\v1\Dashboard\Content\ContentRepository;
use Illuminate\Http\Request;

class PushController extends Controller
{
    private $content;
    function __construct(ContentRepository $repository)
    {
        $this->content = $repository;
    }

    public function pushContent(Request $request){
        $this->content->pushContent(json_decode($request['content'],true), $request['topic_id']);
    }
}
