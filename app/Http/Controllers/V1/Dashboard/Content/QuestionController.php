<?php

namespace App\Http\Controllers\V1\Dashboard\Content;

use App\Http\Controllers\Controller;
use App\Repositories\v1\Dashboard\Content\ContentRepository;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    protected $question;
    function __construct(ContentRepository $repository)
    {
        $this->question = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('topic')){
            return $this->showOne($this->question->topicQuestions($request['topic']));
        }
        return $this->question->all();
    }


    public function store(Request $request)
    {
     // header('Access-Control-Allow-Origin: *');
      if($request->hasFile('upload')){
          return $this->question->createWithFile($request);
      }else{
          return $this->question->create($request->all());
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->question->delete($id);
    }
}
