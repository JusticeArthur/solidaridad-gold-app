<?php

namespace App\Http\Controllers\V1\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $is_searchable = $request->has('search') && strlen($request['search']) > 0;
        if(!$is_searchable){
            return $this->showOne(User::with(['community','community.district','community.district.region','community.district.region.country'])
                ->orderBy('created_at','desc')
                ->paginate($request->size));
        }
        $searchItems = User::search($request['search'])->get()->pluck('id');
        return $this->showOne(User::with('district')->whereIn('id',$searchItems)->latest()->paginate($request['size']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = new User();
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->other_name=$request->other_name;
        $user->username=$request->username;
        $user->email=$request->email;
        $user->gender=$request->gender;
        $user->address=$request->address;
        $user->dob = Carbon::parse($request->dob)->toDateString();
        $user->community_id=$request->community_id;
        $user->status = 'active';
        $user->password = bcrypt('12345678');
        //$user->key = str::uuid();
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $userdob = Carbon::parse($request->dob)->toDateString();
        DB::table('users')
              ->where('id', $id)
              ->update([
                  'first_name' => $request->first_name,
                  'last_name' => $request->last_name,
                  'other_name' =>$request->other_name,
                  'username' => $request->username,
                  'email' => $request->email,
                  'dob' => $userdob,
                  'gender' => $request->gender,
                  'address' => $request->address,
                  'community_id'=>$request->community_id, 
                  ]);


                  return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
           
        Topic::find($id)->delete();
        return $this->showOne('success');
    }
}
