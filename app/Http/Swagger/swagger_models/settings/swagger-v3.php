<?php
/**
 * @SWG\Swagger(
 *     basePath="/api/v1",
 *     schemes="http",
 *     host=L5_SWAGGER_CONST_HOST,
 *      * @OA\Info(
 *     description="This is an API documentation for solidaridad Gold Applicaiton",
 *     version="1.0.0",
 *     title="SOLIDARIDAD GOLD APP",
 *     termsOfService="http://swagger.io/terms/",
 *     @OA\Contact(
 *         email="justicea83@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * )
 */
