<?php


namespace App\Repositories\v1\Dashboard\Content;

use App\Imports\QuestionsImport;
use App\Models\AssociatedTask;
use App\Models\Content;
use App\Models\Question;
use App\Models\Response;
use App\Repositories\RepositoryInterface;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ContentRepository implements RepositoryInterface
{
    use ApiResponser;


    public function all()
    {
        return $this->showOne(Question::with(['responses', 'task'])->latest()->paginate(15));
    }

    public function create(array $data)
    {
        //validate request
        $question = new Question();
        $question->topic_id = $data['topic_id'];
        $question->question = $data['question'];
        $question->question_type = $data['question_type'];
        $question->strong_point = $data['strong_point'];
        $question->weak_point = $data['weak_point'];
        $question->weight = $data['weight'];

        $question->save();

        foreach ($data['responses'] as $responses) {
            $response = new Response();
            $response->question_id = $question->id;
            $response->possible_response = $responses['response'];
            $response->correct = $responses['correct'];
            $response->save();
        }

        if (isset($data['task'])) {
            $task = new AssociatedTask();
            $task->question_id = $question->id;
            $task->task = $data['task'];
            $task->priority = $data['priority'];
            $task->save();
        }

        return $this->showOne('success');
    }

    public function createWithFile(Request $request)
    {
        Excel::import(new QuestionsImport(), $request->file('upload'));
        return $this->showOne('success');
    }

    public function update(array $data, $id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        $question = Question::query()->find($id);
        if ($question) {
            $question->responses()->delete();
            $question->task()->delete();
            $question->delete();
        }
        return $this->showOne('success');
    }

    public function show($id)
    {
        // TODO: Implement show() method.
    }

    public function topicQuestions($topicId){
        $content = Content::query()->where('topic_id',$topicId)->latest()->pluck('question_id');
        return [
            'questions' => Question::query()->where('topic_id',$topicId)->whereNotIn('id',$content)->latest()->paginate(10),
            'content' => Question::query()->whereIn('id',$content)->paginate(10)
        ];
    }

    public function pushContent($content,$topicId){
        $userId = auth()->id();
        Content::query()->where('topic_id',$topicId)->delete();
        foreach ($content as $item){
            try{
                Content::create([
                    'question_id' => $item,
                    'created_by' => $userId,
                    'topic_id' => $topicId
                ]);
            }catch (\Exception $e){}
        }
    }
}
