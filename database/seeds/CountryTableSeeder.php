<?php

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(Country::all()) <= 0 ){
            $countries = json_decode(file_get_contents(public_path() . '/files/countries.json'),true);
            foreach ($countries as $country){
                try{
                    $createdCountry = Country::firstOrCreate([
                        'name' => $country['countryName'],
                        'code' => $country['countryShortCode']
                    ]);
                }catch (Exception $e){
                    continue;
                }


                foreach ($country['regions'] as $region){
                    try{
                        Region::firstOrCreate([
                            'name' => $region['name'],
                            'country_id' => $createdCountry->id
                        ]);
                    }catch (Exception $e){
                        continue;
                    }
                }
            }
            //
            //temp
            $testCountries = Country::all();
            $withCodes = json_decode(file_get_contents(public_path() . '/files/codes.json'),true);
            foreach ($testCountries as $country){
                foreach ($withCodes as $withCode){
                    if(strtolower($country['code']) === strtolower($withCode['code'])){
                        /*dd([
                           $withCode,$country
                        ]);*/
                        $country['dial_code'] = $withCode['dial_code'];
                        try{
                            $country->save();
                        }catch (\Exception $e){

                        }
                    }
                }
            }
        }



    }
}
