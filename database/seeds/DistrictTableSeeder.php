<?php

use App\Models\Country;
use App\Models\District;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = Country::with('regions')->where('code','GH')->first();
        //dd($country->toArray());
        $districts = json_decode(file_get_contents(public_path() . '/files/districts.json'),true);
        //dd($districts);
            foreach ($country->regions as $region){
                foreach ($districts as $district){
                    if(str_replace('-',' ',strtolower($region->name)) === strtolower($district['region'])){
                        /*dd([
                            'district' => $district,
                            'region' => $region->toArray()
                        ]);*/
                        District::create([
                           'name' => str_replace('[','',strtolower($district['district'])),
                           'region_id' => $region->id
                        ]);
                    }
                }
            }

    }
}
