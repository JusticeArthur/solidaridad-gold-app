<?php

use App\Models\Region;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(User::all()) < 1){
            $user = User::firstOrCreate([
              //  'key' => str::uuid(),
                'first_name' => 'Justice',
                'last_name' => 'Arthur',
                'username' => 'justicea83',
                'gender' => 'male',
                'password' => bcrypt('password'),
            ]);

            $role = Role::whereName('super')->first();
            $user->assignRole($role);
        }

    }
}
