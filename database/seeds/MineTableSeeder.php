<?php

use App\Models\Community;
use App\Models\Mine;
use Illuminate\Database\Seeder;
class MineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        if(count(Mine::all()) < 1){
            $faker = Faker\Factory::create();
            $communities = Community::query()->get()->pluck('id');
            for ($i = 0 ; $i < 50; $i ++){
                Mine::create([
                    'name' => $faker->company,
                    'community_id' => $communities[random_int(0,count($communities)-1)],
                    'size' => random_int(2000,20000),
                    'mapped' => random_int(0,1),
                    'address' => $faker->address,
                    'license_exp_date' => now()->addMonths(10)
                ]);
            }
        }

    }
}
