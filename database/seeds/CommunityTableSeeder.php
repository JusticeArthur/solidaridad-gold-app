<?php

use App\Models\Community;
use App\Models\District;
use Illuminate\Database\Seeder;

class CommunityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $districts = District::query()->get()->pluck('id');
        if(count(Community::all()) < 1){
            $faker = Faker\Factory::create();
            for ($i = 0; $i < 30; $i++){
                Community::create([
                    'name' => $faker->streetName,
                    'district_id' => random_int(1,count($districts))
                ]);
            }
        }


    }
}
