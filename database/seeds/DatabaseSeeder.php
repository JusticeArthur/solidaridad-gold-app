<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(DistrictTableSeeder::class);
        $this->call(CommunityTableSeeder::class);
        $this->call(MineTableSeeder::class);
    }
}
