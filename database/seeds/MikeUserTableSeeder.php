<?php

use App\Models\User;
use Illuminate\Database\Seeder;

use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class MikeUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $myuser = new User();
        $myuser->first_name = 'Michael';
       // $myuser->key = str::uuid();
        $myuser->last_name = 'Awortwe';
        $myuser->username = 'Miawortwe';
        $myuser->gender = 'male';
        $myuser->status = 'active';
        $myuser->password = bcrypt('12345678');
        $myuser->save();
        $myrole = Role::where('id',1)->first();
        $myuser->assignRole($myrole);
    }
}
