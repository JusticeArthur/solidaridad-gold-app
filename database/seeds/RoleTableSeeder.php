<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(count(Role::all()) < 2){
            Role::firstOrCreate([
                'name' => 'super',
                'guard_name' => 'api'
            ]);
            Role::firstOrCreate([
                'name' => 'admin',
                'guard_name' => 'api'
            ]);
        }
        if(count(Role::all()) < 4){
            Role::firstOrCreate([
                'name' => 'miner',
                'guard_name' => 'miners'
            ]);
        }
    }
}
