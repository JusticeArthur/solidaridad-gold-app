<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMinersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username')->unique();
            $table->string('other_name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->enum('gender',['male','female','other'])->default('other');
            $table->enum('marital_status',['married','single','divorced','widower/widow','separated','co-habitation','complicated'])->nullable();
            $table->bigInteger('community_id')->nullable();
            $table->string('address')->nullable();
            $table->enum('status',['permanent','casual','inactive'])->default('permanent');
            $table->boolean('profiled')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->softDeletes();
            $table->foreign('community_id')->references('id')->on('communities');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('miners');
    }
}
