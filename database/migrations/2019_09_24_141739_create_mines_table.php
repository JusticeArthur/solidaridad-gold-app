<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->enum('ownership',['sole_proprietorship','company','cooperative','community_mining'])->default('sole_proprietorship');
            $table->bigInteger('community_id')->nullable();
            $table->string('address')->nullable();
            $table->double('size')->nullable();
            $table->boolean('mapped')->default(true);
            $table->date('license_exp_date')->nullable();
            $table->softDeletes();
            $table->foreign('community_id')->references('id')->on('communities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mines');
    }
}
