<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMineMapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mine_maps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('mine_id');
            $table->string('lat')->nullable();
            $table->string('lgt')->nullable();
            $table->string('accuracy')->nullable();
            $table->string('altitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mine_maps');
    }
}
