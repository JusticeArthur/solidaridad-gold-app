<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question');
            $table->bigInteger('topic_id');
            $table->string('question_type')->nullable();
            $table->longText('strong_point')->nullable();
            $table->longText('weak_point')->nullable();
            $table->double('weight')->nullable();
            $table->enum('status',['active','inactive'])->default('inactive');
            $table->unique(['topic_id','question']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
