<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMineManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mine_managers', function (Blueprint $table) {
            $table->bigIncrements('id');
            //mine manager
            $table->bigInteger('miner_id');
            //name of mines
            $table->bigInteger('mine_id');
            $table->unique(['miner_id','mine_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mine_managers');
    }
}
