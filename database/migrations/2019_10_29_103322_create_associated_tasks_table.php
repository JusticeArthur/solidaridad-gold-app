<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssociatedTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associated_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('task');
            $table->bigInteger('question_id');
         //   $table->enum('status',['not_started','progress','completed'])->default('not_started');
            $table->enum('priority',['urgent','important','recommended'])->default('recommended');
           // $table->date('completion_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associated_tasks');
    }
}
