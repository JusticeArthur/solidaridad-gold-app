<?php

use App\Http\Controllers\V1\Dashboard\Content\QuestionController;
use App\Http\Controllers\V1\Dashboard\settings\CommunityController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::namespace('V1')->prefix('v1')->group(function (){
    Route::get('users','Test\TestController@users');
    Route::post('sign-in','Auth\AuthController@signIn');
    Route::post('verify-phone','Auth\AuthController@verifyPhone');
    Route::post('verify-email','Auth\AuthController@verifyEmail');
    Route::post('register-mine','Auth\AuthController@registerMiner');
    Route::get('countries','Dashboard\Settings\MiscController@countries');
    Route::get('country','Dashboard\Settings\MiscController@country');
    Route::get('mines','Dashboard\Settings\MiscController@mines');
    Route::middleware(['auth:api,miners'])->group(function (){
        Route::post('logout','Auth\AuthController@logout');
    });

    Route::namespace('Dashboard')->middleware(['auth:api,miners'])->prefix('dashboard')->group(function (){
        Route::namespace('Settings')->prefix('settings')->group(function (){
            Route::resource('regions','RegionController');
            Route::group(['prefix'=> 'misc'],function (){
                Route::get('countries','MiscController@countries');
                Route::get('get-users','MiscController@users');
                Route::get('pillars/{id}','MiscController@pillars');
                Route::get('excel-export-regions','MiscController@exportRegions');
                Route::get('excel-export-topics','MiscController@exportTopics');
                Route::get('excel-export-mines','MiscController@exportMines');
                Route::get('excel-export-users','MiscController@exportUsers');
                Route::get('excel-export-districts','MiscController@exportDistricts');
                Route::get('excel-export-communities','MiscController@exportCommunities');
                Route::get('excel-export-educations','MiscController@exportEducations');
                Route::get('excel-export-pillars','MiscController@exportPillars');
                Route::get('excel-export-programs','MiscController@exportPrograms');
                Route::get('regions/{id}','MiscController@regions');
                Route::get('districts/{id}','MiscController@districts');
                Route::get('permissions','MiscController@permissions');
                Route::get('community/{id}','MiscController@community');
                Route::get('topics/{id}','MiscController@topics');
                Route::get('counts','MiscController@count');
            });
            Route::resource('district','DistrictController');
            Route::resource('programs','ProgramController');
            Route::resource('pillars','PillarController');
            Route::resource('education','EducationController');
            Route::resource('topics','TopicController');
            Route::resource('mines','MinesController');
            Route::resource('community','CommunityController');
            Route::resource('roles','RolesController');
        });
        Route::namespace('Content')->prefix('content')->group(function(){
            Route::resource('question','QuestionController');
            Route::post('push-content','PushController@pushContent');
        });
        Route::resource('users','UsersController');
    });
    Route::namespace('Services')->middleware(['auth:api,miners'])->prefix('services')->group(function (){
        Route::get('load-pillars','LoadUserInfoController@loadPillars');
    });
});
